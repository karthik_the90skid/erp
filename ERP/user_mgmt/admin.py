from django.contrib import admin
from .models import *

admin.site.register(EmployeeRegister)
admin.site.register(FingerPrintData)
admin.site.register(Access)
# Register your models here.
