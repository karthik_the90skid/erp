from django.shortcuts import render
from django.http import HttpResponse
from . models import *
from django.contrib.auth.models import User
import datetime

def index(request):
    return render(request,"index.html")

def home(request):
    return render(request,"index.html",{"cover":"ERP","show":True})

def employee_register(request):
    if request.method=="POST":
        user_name = request.POST["fname"]
        user_id = request.POST["userid"]
        password = request.POST["pwd"]
        aadhar_no = request.POST["anumber"]
        pan_no = request.POST["pnumber"]
        fingerprint_id =request.POST["fpid"]
        mail_id =request.POST["email"]
        role = request.POST["role"]
        phone_number = request.POST["mnumber"]
        door_no = request.POST["dnumber"]
        street = request.POST["street"]
        city = request.POST["city"]
        state = request.POST["state"]
        zip = request.POST["zip"]
        address = door_no+", "+street+", "+city+", "+state+", "+zip
        user = User.objects.create_user(user_name,  mail_id , password)
        user.save()
        db = EmployeeRegister(user_data=user,user_id=user_id,user_name=user_name,aadhar_no=aadhar_no,
                              pan_no=pan_no ,fingerprint_id=fingerprint_id,mail_id=mail_id,role=role,
                              phone_number=phone_number,address =address,updated_ts=datetime.datetime.now())
        db.save()

        return HttpResponse("Success")



    return render(request,"employee_register.html")


def user_access(request):
    keys = ("umgmt","emgmt","inward" )
    answer = []
    if request.method == "POST":

        role = request.POST["role"]
        for k in keys:
            try:
                 request.POST[k]
                 answer.append(True)
            except Exception as e:
                answer.append(False)
        print(request.POST)
        print(answer)
        db = Access(role=role,u_mgmt=answer[0],e_mgmt =answer[1],stock_inward = answer[2])
        db.save()
        return HttpResponse("Success")

    return render(request,"user_management.html")

def finger_print(request):
    fingerprint_id = models.CharField(max_length=10)
    created_time = models.DateTimeField(auto_created=True)
    created_ts = models.DateTimeField(auto_created=True)
    updated_ts = models.DateTimeField(auto_now=True)

# Create your views here.
