from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class EmployeeRegister(models.Model):

    user_data = models.OneToOneField(User, on_delete=models.CASCADE)
    user_id = models.CharField(max_length=10)
    user_name = models.CharField(max_length=20)
    aadhar_no = models.CharField(max_length=12)
    pan_no = models.CharField(max_length=10)
    fingerprint_id = models.CharField(max_length=10)
    mail_id = models.CharField(max_length=30)
    role = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=15)
    address = models.CharField(max_length=100)
    created_ts = models.DateTimeField(auto_now = True )
    updated_ts = models.DateTimeField()

    def __str__(self):
        return self.user_name


class FingerPrintData(models.Model):

    fingerprint_id = models.CharField(max_length=20)
    user_id = models.CharField(max_length=10,default="")
    created_ts = models.DateTimeField(auto_now=True)
    updated_ts = models.DateTimeField()

    def __str__(self):
        return self.fingerprint_id

class Access(models.Model):

    role = models.CharField(max_length=20)
    u_mgmt = models.BooleanField(default=False)
    e_mgmt = models.BooleanField(default=False)
    stock_inward = models.BooleanField(default=False)

    def __str__(self):
        return self. role

